<img src="/assets/logo.png" align="center" width="300px" alt="Rusty CPR Toolbox logo">

# Rusty Cyberpunk RED GM Toolbox

This project is aimed at Game Masters playing the Cyberpunk RED TTRPG created by Mike Pondsmith,
property of RTalsorian Games.

Bits and parts were taken from the [eframe_template repo](https://github.com/emilk/eframe_template) from  Emil Ernerfeldt for the base project, and the [egui_glow_multiwin repo](https://github.com/vivlim/egui-glow-multiwin) from Vivian Lim, a proof-of-concept multi-window egui application.

The Rust and egui basics were taught to me by Maël Martin.

# I. Getting started

## I.1. Installing Rust and Cargo

This application is written in pure Rust and may be build with Cargo.
You will first need to install it following [this guide](https://doc.rust-lang.org/cargo/getting-started/installation.html).
Reading further documentation on Rust is not required to use this application.

## I.2. MongoDB

The Cyberpunk RED Toolbox uses MongoDB to store data such as characters sheets or NET architectures.
You will need to install it and configure it locally.

### Windows, MacOS and most Linux distributions

Follow the [installation guide](https://www.mongodb.com/docs/manual/administration/install-community/) from the MongoDB official website.

### Arch-based Linux distributions

MongoDB has been pulled out of the official repositories, and you will need to install it via AUR.
First, install the `mongodb-bin` and `mongosh-bin` packages (press ENTER for any prompt yay throws at you).

```
yay -Syu mongodb-bin mongosh-bin
sudo cyctemctl enable mongodb
sudo systemctl restart mongodb
```

### Configure MongoDB locally

You can find the MongoDB configuration file following [these instructions](https://www.mongodb.com/docs/manual/reference/configuration-options/) depending on your OS.
You can change various parameters, but the one that will most probably interest you is the address and port.

```yaml
# network interfaces
net:
  port: 27017
  bindIp: 127.0.0.1
```

If you have a remote instance on a server, you can enter its address and the port used, and skip the rest of this section.

If you want to use it locally, however, you can leave the config as is.
You will also need to remove the socket file just in case, as permissions on it are often wrong:

```
sudo rm -rf /tmp/mongodb-<port>.sock
sudo systemctl restart mongodb
```

Next create a new database and user for it by entering the following commands in the terminal:

```
mongosh
[some very verbose output]
use CPR;
db.createUser( {
    user: "toto",
    pwd:  "toto",
    roles: [
        { role: "readWrite", db: "config" }
    ]
} );
```

As you can see, we have created a new user "toto" with password "toto" and gave it read/write and config access on the database "CPR".
You can change this as you like, but write it somewhere, as you will need it for the Toolbox settings. You could also want to edit the database manually, in which case you would need to connect to it with the following command:

```
mongosh --host 127.0.0.1 --port 27017 -u toto -p toto --authenticationDatabase CPR
```

## I.3.  Building the Cyberpunk RED Toolbox

This piece of software is distributed as-is, and will need to be compiled.
You can do so by typing the following command in a terminal from the root of the project:

```
cargo build -r --bin=cpr-gm-screen
```

You will then find the program under `target/release/cpr-gm-screen`, which you will be able to run.

## I.4. Web deploy

Deploying this application on a website may be possible in the future, as egui is capable of running off the web out of the box, but isn't currently supported by the project.

# II. Features

## II.1. Jukebox

TODO!

## II.2. A handful of rules

TODO!

## II.3. Turn Order

TODO!

## II.4. NET Architectures

TODO!

## II.5. Showing maps and images

TODO!

## II.6. Character Sheets

TODO!
