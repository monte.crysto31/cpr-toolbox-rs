// local
use crate::
{
    utils,
};

// std
use std::
{
    path::Path,
};

// extern
use lazy_static::lazy_static;
use egui_extras::RetainedImage;

// Load all textures only one

lazy_static!
{
    // Global

    pub static ref TEX_LOGO:    RetainedImage = utils::load_image_from_path("assets/logo.png").unwrap();
    pub static ref TEX_BUTTON:  RetainedImage = utils::load_image_from_path("assets/gui/button/idle_background.png").unwrap();

    // NET

    pub static ref TEX_NET_FLOOR:   RetainedImage = utils::load_image_from_path("assets/gui/net/floor_idle.png").unwrap();
    pub static ref TEX_NET_VLINE:   RetainedImage = utils::load_image_from_path("assets/gui/net/vertical_line.png").unwrap();
    pub static ref TEX_NET_HLINE:   RetainedImage = utils::load_image_from_path("assets/gui/net/horizontal_line.png").unwrap();
    pub static ref TEX_NET_TURN:    RetainedImage = utils::load_image_from_path("assets/gui/net/turn_line.png").unwrap();
}