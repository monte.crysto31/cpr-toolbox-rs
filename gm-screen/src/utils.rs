// local


// std
use std::{
    error::Error,
    time,
    path::Path,
};

// extern
use image;
use mongodb::{
    options::ClientOptions,
    {
        sync::{
            Client,
            Database,
        }
    }
};
use rand::{self, Rng};
use egui_extras::RetainedImage;

// ROLLS

pub fn roll_sum(rolls: Vec<u8>, nb_dice: u8) -> u8
{
    return rolls.into_iter().sum();
}

pub fn roll(nb_dice: u8, nb_sides: u8) -> Vec<u8>
{
    if nb_dice == 0
    {
        println!("Cannot roll 0 die!");
        return Vec::new();
    }

    let mut rolls = Vec::new();
    let mut rng = rand::thread_rng();

    // print!("Rolling {}d{}: [ ", nb_dice, nb_sides);
    for d in 0..nb_dice
    {
        rolls.push(rng.gen_range(1..(nb_sides + 1)));
        // print!("{} ", rolls.get(usize::from(d)).unwrap());
    }
    // print!("]\n");

    // let mut tmp = rolls.to_vec();
    // println!("Total: {}", roll_sum(tmp, nb_dice));

    return rolls;
}

pub fn roll_total(nb_dice: u8, nb_sides: u8) -> u8
{
    return roll(nb_dice, nb_sides).into_iter().sum();
}

// TEXTURES

pub fn load_image_from_path(path: &str) -> Result<RetainedImage, image::ImageError>
{
    let image = image::io::Reader::open(Path::new(path))?.decode()?;
    let size = [image.width() as _, image.height() as _];
    let image_buffer = image.to_rgba8();
    let pixels = image_buffer.as_flat_samples();
    
    Ok(RetainedImage::from_color_image(path, egui::ColorImage::from_rgba_unmultiplied(size,pixels.as_slice())))
}

// MONGODB

fn db_connect_result(address: &str, name: &str, user: &str, pwd: &str) -> Result<Database, Box<dyn Error>>
{
    // setup ClientOptions
    let mut client_options = ClientOptions::parse(format!("mongodb://{address}"))?;
    client_options.app_name = Some("Cyberpunk RED Toolbox".to_string());
    client_options.connect_timeout = Some(time::Duration::new(1, 0));

    // get a Client handle
    let client = Client::with_options(client_options)?;
    let db = client.database(name);

    // try to do something to force Error if database unreachable
    let tmp = client.list_database_names(None, None);

    return Ok(db);
}

pub fn db_connect(address: &str, name: &str, user: &str, pwd: &str) -> Option<Database>
{
    let db: Result<Database, Box<dyn Error>> = db_connect_result(address, name, user, pwd);

    if db.is_err()
    {
        return None;
    }

    return Some(db.unwrap());
}