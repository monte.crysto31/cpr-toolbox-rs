// Local crate
use crate::{
    net::{self, NetArchitecture},
    utils
};

// std
use std::{
    collections::HashMap,
    path::Path,
};

// extern
use eframe::egui::Color32;
use egui_extras::RetainedImage;
use lazy_static::lazy_static;

// COLORS

pub const BLACK:        Color32 = Color32::from_rgb(0,   0,   0  );
pub const RED:          Color32 = Color32::from_rgb(204, 0,   0  );
pub const ACCENT:       Color32 = Color32::from_rgb(34,  223, 255);
pub const BACKGROUND:   Color32 = Color32::from_rgb(10,  5,   5  );

// TABLES

// start and end indexes of different kind of programs
pub const PROGRAMS_INDEXES:     (u8, u8) = ( 0, 14);
pub const BLACK_ICE_INDEXES:    (u8, u8) = (15, 26);
pub const DEMONS_INDEXES:       (u8, u8) = (27, 29);

lazy_static!
{
    // id -> menu name
    pub static ref MENUS: HashMap<u8, String> =
    {
        let mut res = HashMap::new();
        res.insert(0, "Main Menu".to_string());
        res.insert(1, "Jukebox".to_string());
        res.insert(2, "Rules".to_string());
        res.insert(3, "Turn Order".to_string());
        res.insert(4, "NET Architectures".to_string());
        res.insert(5, "Maps".to_string());
        res.insert(6, "Character Sheets".to_string());
        res.insert(7, "About".to_string());
        res.insert(8, "Settings".to_string());
        res
    };

    // id -> one program
    pub static ref PROGRAMS: HashMap<u64, net::Program> = {
        let mut res = HashMap::new();

        // Boosters
        res.insert(
            0,
            net::Program {
                name:   "Eraser".to_string(),
                nature: "Booster".to_string(),
                target: "".to_string(),
                stats:  (0, 0, 0, 0, 7)
            }
        );
        res.insert(
            1,
            net::Program {
                name:   "See Ya".to_string(),
                nature: "Booster".to_string(),
                target: "".to_string(),
                stats:  (0, 0, 0, 0, 7)
            }
        );
        res.insert(
            2,
            net::Program {
                name:   "Sppedy Gonzalvez".to_string(),
                nature: "Booster".to_string(),
                target: "".to_string(),
                stats:  (0, 0, 0, 0, 7)
            }
        );
        res.insert(
            3,
            net::Program {
                name:   "Worm".to_string(),
                nature: "Booster".to_string(),
                target: "".to_string(),
                stats:  (0, 0, 0, 0, 7)
            }
        );

        // Defenders
        res.insert(
            4,
            net::Program {
                name:   "Armor".to_string(),
                nature: "Defender".to_string(),
                target: "".to_string(),
                stats:  (0, 0, 0, 0, 7)
            }
        );
        res.insert(
            5,
            net::Program {
                name:   "Flak".to_string(),
                nature: "Defender".to_string(),
                target: "".to_string(),
                stats:  (0, 0, 0, 0, 7)
            }
        );
        res.insert(
            6,
            net::Program {
                name:   "Shield".to_string(),
                nature: "Defender".to_string(),
                target: "".to_string(),
                stats:  (0, 0, 0, 0, 7)
            }
        );

        // Attackers
        res.insert(
            7,
            net::Program {
                name:   "Banhammer".to_string(),
                nature: "Attacker".to_string(),
                target: "Anti-Program".to_string(),
                stats:  (0, 0, 1, 0, 0)
            }
        );
        res.insert(
            8,
            net::Program {
                name:   "Sword".to_string(),
                nature: "Attacker".to_string(),
                target: "Anti-Program".to_string(),
                stats:  (0, 0, 1, 0, 0)
            }
        );
        res.insert(
            9,
            net::Program {
                name:   "DeckKRASH".to_string(),
                nature: "Attacker".to_string(),
                target: "Anti-Personnel".to_string(),
                stats:  (0, 0, 0, 0, 0)
            }
        );
        res.insert(
            10,
            net::Program {
                name:   "Hellbolt".to_string(),
                nature: "Attacker".to_string(),
                target: "Anti-Personnel".to_string(),
                stats:  (0, 0, 2, 0, 0)
            }
        );
        res.insert(
            11,
            net::Program {
                name:   "Nervescrub".to_string(),
                nature: "Attacker".to_string(),
                target: "Anti-Personnel".to_string(),
                stats:  (0, 0, 0, 0, 0)
            }
        );
        res.insert(
            12,
            net::Program {
                name:   "Poison Flatline".to_string(),
                nature: "Attacker".to_string(),
                target: "Anti-Personnel".to_string(),
                stats:  (0, 0, 0, 0, 0)
            }
        );
        res.insert(
            13,
            net::Program {
                name:   "Superglue".to_string(),
                nature: "Attacker".to_string(),
                target: "Anti-Personnel".to_string(),
                stats:  (0, 0, 2, 0, 0)
            }
        );
        res.insert(
            14,
            net::Program {
                name:   "Vrizzbolt".to_string(),
                nature: "Attacker".to_string(),
                target: "Anti-Personnel".to_string(),
                stats:  (0, 0, 1, 0, 0)
            }
        );

        // Black ICE
        res.insert(
            15,
            net::Program {
                name:   "Asp".to_string(),
                nature: "Black ICE".to_string(),
                target: "Anti-Personnel".to_string(),
                stats:  (4, 6, 2, 2, 15)
            }
        );
        res.insert(
            16,
            net::Program {
                name:   "Giant".to_string(),
                nature: "Black ICE".to_string(),
                target: "Anti-Personnel".to_string(),
                stats:  (2, 2, 8, 4, 25)
            }
        );
        res.insert(
            17,
            net::Program {
                name:   "Hellhound".to_string(),
                nature: "Black ICE".to_string(),
                target: "Anti-Personnel".to_string(),
                stats:  (6, 6, 6, 2, 25)
            }
        );
        res.insert(
            18,
            net::Program {
                name:   "Kraken".to_string(),
                nature: "Black ICE".to_string(),
                target: "Anti-Personnel".to_string(),
                stats:  (6, 2, 8, 4, 30)
            }
        );
        res.insert(
            19,
            net::Program {
                name:   "Liche".to_string(),
                nature: "Black ICE".to_string(),
                target: "Anti-Personnel".to_string(),
                stats:  (8, 2, 6, 2, 25)
            }
        );
        res.insert(
            20,
            net::Program {
                name:   "Raven".to_string(),
                nature: "Black ICE".to_string(),
                target: "Anti-Personnel".to_string(),
                stats:  (6, 4, 4, 2, 15)
            }
        );
        res.insert(
            21,
            net::Program {
                name:   "Scorpion".to_string(),
                nature: "Black ICE".to_string(),
                target: "Anti-Personnel".to_string(),
                stats:  (2, 6, 2, 2, 15)
            }
        );
        res.insert(
            22,
            net::Program {
                name:   "Skunk".to_string(),
                nature: "Black ICE".to_string(),
                target: "Anti-Personnel".to_string(),
                stats:  (2, 4, 4, 2, 10)
            }
        );
        res.insert(
            23,
            net::Program {
                name:   "Wisp".to_string(),
                nature: "Black ICE".to_string(),
                target: "Anti-Personnel".to_string(),
                stats:  (4, 4, 4, 2, 15)
            }
        );
        res.insert(
            24,
            net::Program {
                name:   "Dragon".to_string(),
                nature: "Black ICE".to_string(),
                target: "Anti-Program".to_string(),
                stats:  (6, 4, 6, 6, 30)
            }
        );
        res.insert(
            25,
            net::Program {
                name:   "Killer".to_string(),
                nature: "Black ICE".to_string(),
                target: "Anti-Program".to_string(),
                stats:  (4, 8, 6, 2, 20)
            }
        );
        res.insert(
            26,
            net::Program {
                name:   "Sabertooth".to_string(),
                nature: "Black ICE".to_string(),
                target: "Anti-Program".to_string(),
                stats:  (8, 6, 6, 2, 25)
            }
        );

        // Demons
        res.insert(
            27,
            net::Program {
                name:   "Imp".to_string(),
                nature: "Demon".to_string(),
                target: "".to_string(),
                stats:  (3, 2, 14, 0, 15)
            }
        );
        res.insert(
            28,
            net::Program {
                name:   "Efreet".to_string(),
                nature: "Demon".to_string(),
                target: "".to_string(),
                stats:  (4, 3, 14, 0, 25)
            }
        );
        res.insert(
            29,
            net::Program {
                name:   "Balron".to_string(),
                nature: "Demon".to_string(),
                target: "".to_string(),
                stats:  (7, 4, 14, 0, 30)
            }
        );

        res
    };

    // roll -> floor contents
    pub static ref NET_LOBBY: HashMap<u8, Vec<net::ArchiItem>> = {
        let mut res = HashMap::new();

        res.insert(
            1,
            vec![
                net::ArchiItem::Challenge {
                    name: "File".to_string(),
                    dd: 6,
                    contents: "".to_string()
                }
            ]
        );
        res.insert(
            2,
            vec![ 
                net::ArchiItem::Challenge {
                    name: "Password".to_string(),
                    dd: 6,
                    contents: "".to_string()
                }
            ]
        );
        res.insert(
            3,
            vec![
                net::ArchiItem::Challenge
                {
                    name: "Password".to_string(),
                    dd: 8,
                    contents: "".to_string()
                }
            ]
        );
        res.insert(
            4,
            vec![
                net::ArchiItem::Program { id: 22 }
            ]
        );
        res.insert(
            5,
            vec![
                net::ArchiItem::Program { id: 23 }
            ]
        );
        res.insert(
            6,
            vec![
                net::ArchiItem::Program { id: 24 }
            ]
        );

        res
    };

    // (roll, diff) -> floor contents
    pub static ref NET_FLOORS: HashMap<(u8, u8), Vec<net::ArchiItem>> = {
        let mut res = HashMap::new();
        
        // basic diff = 1

        res.insert(
            (3, 1),
            vec![
                net::ArchiItem::Program { id: 17 }
            ]
        );
        res.insert(
            (4, 1),
            vec![
                net::ArchiItem::Program { id: 26 }
            ]
        );
        res.insert(
            (5, 1),
            vec![
                net::ArchiItem::Program { id: 20 },
                net::ArchiItem::Program { id: 20 }
            ]
        );
        res.insert(
            (6, 1),
            vec![
                net::ArchiItem::Program { id: 17 }
            ]
        );
        res.insert(
            (7, 1),
            vec![
                net::ArchiItem::Program { id: 23 }
            ]
        );
        res.insert(
            (8, 1),
            vec![
                net::ArchiItem::Program { id: 20 }
            ]
        );
        res.insert(
            (9, 1),
            vec![
                net::ArchiItem::Challenge {
                    name: "Password".to_string(),
                    dd: 6,
                    contents: "".to_string()
                }
            ]
        );
        res.insert(
            (10, 1),
            vec![
                net::ArchiItem::Challenge {
                    name: "File".to_string(),
                    dd: 6,
                    contents: "".to_string()
                }
            ]
        );
        res.insert(
            (11, 1),
            vec![
                net::ArchiItem::Challenge {
                    name: "Control Node".to_string(),
                    dd: 6,
                    contents: "".to_string()
                }
            ]
        );
        res.insert(
            (12, 1),
            vec![
                net::ArchiItem::Challenge {
                    name: "Password".to_string(),
                    dd: 6,
                    contents: "".to_string()
                }
            ]
        );
        res.insert(
            (13, 1),
            vec![
                net::ArchiItem::Program { id: 22 }
            ]
        );
        res.insert(
            (14, 1),
            vec![
                net::ArchiItem::Program { id: 15 }
            ]
        );
        res.insert(
            (15, 1),
            vec![
                net::ArchiItem::Program { id: 21 }
            ]
        );
        res.insert(
            (16, 1),
            vec![
                net::ArchiItem::Program { id: 25 },
                net::ArchiItem::Program { id: 22 }
            ]
        );
        res.insert(
            (17, 1),
            vec![
                net::ArchiItem::Program { id: 23 },
                net::ArchiItem::Program { id: 23 },
                net::ArchiItem::Program { id: 23 }
            ]
        );
        res.insert(
            (18, 1),
            vec![
                net::ArchiItem::Program { id: 19 }
            ]
        );

        // standard diff = 2

        res.insert(
            (3, 2),
            vec![
                net::ArchiItem::Program { id: 17 },
                net::ArchiItem::Program { id: 17 }
            ]
        );
        res.insert(
            (4, 2),
            vec![
                net::ArchiItem::Program { id: 17 },
                net::ArchiItem::Program { id: 25 }
            ]
        );
        res.insert(
            (5, 2),
            vec![
                net::ArchiItem::Program { id: 22 },
                net::ArchiItem::Program { id: 22 }
            ]
        );
        res.insert(
            (6, 2),
            vec![
                net::ArchiItem::Program { id: 26 }
            ]
        );
        res.insert(
            (7, 2),
            vec![
                net::ArchiItem::Program { id: 21 }
            ]
        );
        res.insert(
            (8, 2),
            vec![
                net::ArchiItem::Program { id: 17 }
            ]
        );
        res.insert(
            (9, 2),
            vec![
                net::ArchiItem::Challenge {
                    name: "Password".to_string(),
                    dd: 8,
                    contents: "".to_string()
                }
            ]
        );
        res.insert(
            (10, 2),
            vec![
                net::ArchiItem::Challenge {
                    name: "File".to_string(),
                    dd: 8,
                    contents: "".to_string()
                }
            ]
        );
        res.insert(
            (11, 2),
            vec![
                net::ArchiItem::Challenge {
                    name: "Control Node".to_string(),
                    dd: 8,
                    contents: "".to_string()
                }
            ]
        );
        res.insert(
            (12, 2),
            vec![
                net::ArchiItem::Challenge {
                    name: "Password".to_string(),
                    dd: 8,
                    contents: "".to_string()
                }
            ]
        );
        res.insert(
            (13, 2),
            vec![
                net::ArchiItem::Program { id: 15 }
            ]
        );
        res.insert(
            (14, 2),
            vec![
                net::ArchiItem::Program { id: 25 }
            ]
        );
        res.insert(
            (15, 2),
            vec![
                net::ArchiItem::Program { id: 19 }
            ]
        );
        res.insert(
            (16, 2),
            vec![
                net::ArchiItem::Program { id: 15 }
            ]
        );
        res.insert(
            (17, 2),
            vec![
                net::ArchiItem::Program { id: 20 },
                net::ArchiItem::Program { id: 20 },
                net::ArchiItem::Program { id: 20 }
            ]
        );
        res.insert(
            (18, 2),
            vec![
                net::ArchiItem::Program { id: 19 },
                net::ArchiItem::Program { id: 20 }
            ]
        );

        // uncommon diff = 3

        res.insert(
            (3, 3),
            vec![
                net::ArchiItem::Program { id: 18 }
            ]
        );
        res.insert(
            (4, 3),
            vec![
                net::ArchiItem::Program { id: 17 },
                net::ArchiItem::Program { id: 21 }
            ]
        );
        res.insert(
            (5, 3),
            vec![
                net::ArchiItem::Program { id: 17 },
                net::ArchiItem::Program { id: 25 }
            ]
        );
        res.insert(
            (6, 3),
            vec![
                net::ArchiItem::Program { id: 20 },
                net::ArchiItem::Program { id: 20 }
            ]
        );
        res.insert(
            (7, 3),
            vec![
                net::ArchiItem::Program { id: 26 }
            ]
        );
        res.insert(
            (8, 3),
            vec![
                net::ArchiItem::Program { id: 17 }
            ]
        );
        res.insert(
            (9, 3),
            vec![
                net::ArchiItem::Challenge {
                    name: "Password".to_string(),
                    dd: 10,
                    contents: "".to_string()
                }
            ]
        );
        res.insert(
            (10, 3),
            vec![
                net::ArchiItem::Challenge {
                    name: "File".to_string(),
                    dd: 10,
                    contents: "".to_string()
                }
            ]
        );
        res.insert(
            (11, 3),
            vec![
                net::ArchiItem::Challenge {
                    name: "Control Node".to_string(),
                    dd: 10,
                    contents: "".to_string()
                }
            ]
        );
        res.insert(
            (12, 3),
            vec![
                net::ArchiItem::Challenge {
                    name: "Password".to_string(),
                    dd: 10,
                    contents: "".to_string()
                }
            ]
        );
        res.insert(
            (13, 3),
            vec![
                net::ArchiItem::Program { id: 25 }
            ]
        );
        res.insert(
            (14, 3),
            vec![
                net::ArchiItem::Program { id: 19 }
            ]
        );
        res.insert(
            (15, 3),
            vec![
                net::ArchiItem::Program { id: 24 }
            ]
        );
        res.insert(
            (16, 3),
            vec![
                net::ArchiItem::Program { id: 15 },
                net::ArchiItem::Program { id: 20 }
            ]
        );
        res.insert(
            (17, 3),
            vec![
                net::ArchiItem::Program { id: 24 },
                net::ArchiItem::Program { id: 23 }
            ]
        );
        res.insert(
            (18, 3),
            vec![
                net::ArchiItem::Program { id: 16 }
            ]
        );

        // advanced diff = 4

        res.insert(
            (3, 4),
            vec![
                net::ArchiItem::Program { id: 17 },
                net::ArchiItem::Program { id: 17 },
                net::ArchiItem::Program { id: 17 }
            ]
        );
        res.insert(
            (4, 4),
            vec![
                net::ArchiItem::Program { id: 15 },
                net::ArchiItem::Program { id: 15 }
            ]
        );
        res.insert(
            (5, 4),
            vec![
                net::ArchiItem::Program { id: 17 },
                net::ArchiItem::Program { id: 19 }
            ]
        );
        res.insert(
            (6, 4),
            vec![
                net::ArchiItem::Program { id: 23 },
                net::ArchiItem::Program { id: 23 },
                net::ArchiItem::Program { id: 23 }
            ]
        );
        res.insert(
            (7, 4),
            vec![
                net::ArchiItem::Program { id: 17 },
                net::ArchiItem::Program { id: 26 }
            ]
        );
        res.insert(
            (8, 4),
            vec![
                net::ArchiItem::Program { id: 18 }
            ]
        );
        res.insert(
            (9, 4),
            vec![
                net::ArchiItem::Challenge {
                    name: "Password".to_string(),
                    dd: 12,
                    contents: "".to_string()
                }
            ]
        );
        res.insert(
            (10, 4),
            vec![
                net::ArchiItem::Challenge {
                    name: "File".to_string(),
                    dd: 12,
                    contents: "".to_string()
                }
            ]
        );
        res.insert(
            (11, 4),
            vec![
                net::ArchiItem::Challenge {
                    name: "Control Node".to_string(),
                    dd: 12,
                    contents: "".to_string()
                }
            ]
        );
        res.insert(
            (12, 4),
            vec![
                net::ArchiItem::Challenge {
                    name: "Password".to_string(),
                    dd: 12,
                    contents: "".to_string()
                }
            ]
        );
        res.insert(
            (13, 4),
            vec![
                net::ArchiItem::Program { id: 16 }
            ]
        );
        res.insert(
            (14, 4),
            vec![
                net::ArchiItem::Program { id: 24 }
            ]
        );
        res.insert(
            (15, 4),
            vec![
                net::ArchiItem::Program { id: 25 },
                net::ArchiItem::Program { id: 21 }
            ]
        );
        res.insert(
            (16, 4),
            vec![
                net::ArchiItem::Program { id: 18 }
            ]
        );
        res.insert(
            (17, 4),
            vec![
                net::ArchiItem::Program { id: 20 },
                net::ArchiItem::Program { id: 23 },
                net::ArchiItem::Program { id: 17 }
            ]
        );
        res.insert(
            (18, 4),
            vec![
                net::ArchiItem::Program { id: 24 },
                net::ArchiItem::Program { id: 24 }
            ]
        );

        res
    };
}

pub static mut NET: Option<net::NetArchitecture> = None;