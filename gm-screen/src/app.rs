// local crate
use crate::{
    utils,
    tex,
    globals,
    net,
};

// std
use std::{
    u8,
    error::Error,
};

use egui::ScrollArea;
// extern crates
use serde::{
    Serialize,
    Deserialize,
};
use mongodb::sync::Database;
use tokio;

// We derive Deserialize/Serialize so we can persist app state on shutdown.
#[derive(Deserialize, Serialize)]
// #[serde(default)] // if we add new fields, give them default values when deserializing old state
pub struct TemplateApp
{
    // #[serde(skip)]
    // cpr_frame: egui::containers::Frame,
    is_fullscreen:  bool,
    first_frame:    bool,

    // Settings
    resolution:     f32,
    ui_scale:       f32,
    music_volume:   f32,
    sfx_volume:     f32,
    db_address:     String,
    db_name:        String,
    db_user:        String,
    db_pwd:         String,

    // App state
    menu_id:        u8,
    #[serde(skip)]
    db_handle:      Option<Database>,
    curr_net:       Option<net::NetArchitecture>,
}

impl Default for TemplateApp
{
    fn default() -> Self
    {
        /* TODO: find a way to make that work
        let cpr_frame = egui::containers::Frame::default()
            .fill(constants::BACKGROUND)
            .rounding(egui::Rounding {nw: 0.0, ne: 0.0, sw: 0.0, se: 0.0})
            .stroke(egui::Stroke::new(2.0, constants::RED))
            .inner_margin(egui::style::Margin {left: 10., right: 10., top: 10., bottom: 10.})
            .outer_margin(egui::style::Margin {left: 0., right: 0., top: 0., bottom: 0.});
        */

        Self
        {
            // init App attributes
            // cpr_frame,
            is_fullscreen:  false,
            first_frame:    true,
            
            // TODO: read the settings from a config.json to not rely on autosaves
            resolution:     1080.0f32,
            ui_scale:       1f32,
            music_volume:   1f32,
            sfx_volume:     1f32,
            db_address:     "127.0.0.1:27017".to_string(),
            db_name:        "CPR".to_string(),
            db_user:        "toto".to_string(),
            db_pwd:         "toto".to_string(),

            menu_id:        0u8,
            db_handle:      None,
            curr_net:       None,
        }
    }
}

impl TemplateApp
{
    // Called once before the first frame.
    pub fn new(cc: &eframe::CreationContext<'_>) -> Self
    {
        // This is also where you can customized the look at feel of egui
        let mut visuals = egui::Visuals::dark();

        // Padding
        visuals.window_rounding                 = egui::Rounding::none();
        visuals.widgets.active.rounding         = egui::Rounding::none();
        visuals.widgets.noninteractive.rounding = egui::Rounding::none();
        visuals.widgets.inactive.rounding       = egui::Rounding::none();
        visuals.widgets.hovered.rounding        = egui::Rounding::none();
        visuals.widgets.open.rounding           = egui::Rounding::none();

        // Colors
        visuals.hyperlink_color                 = globals::ACCENT;
        visuals.override_text_color             = Some(globals::RED);
        visuals.faint_bg_color                  = globals::BACKGROUND;
        visuals.extreme_bg_color                = globals::BLACK;

        cc.egui_ctx.set_visuals(visuals);

        // Fonts
        // cc.egui_ctx.set_fonts;

        // Text styles
        let mut style = (*cc.egui_ctx.style()).clone();
        style.text_styles = [
            (egui::TextStyle::Heading, egui::FontId::new(30.0, egui::FontFamily::Proportional)),
            (egui::TextStyle::Name("Heading2".into()), egui::FontId::new(25.0, egui::FontFamily::Proportional)),
            (egui::TextStyle::Name("ContextHeading".into()), egui::FontId::new(25.0, egui::FontFamily::Proportional)),
            (egui::TextStyle::Body, egui::FontId::new(20.0, egui::FontFamily::Proportional)),
            (egui::TextStyle::Monospace, egui::FontId::new(20.0, egui::FontFamily::Proportional)),
            (egui::TextStyle::Button, egui::FontId::new(25.0, egui::FontFamily::Proportional)),
            (egui::TextStyle::Small, egui::FontId::new(15.0, egui::FontFamily::Proportional)),
        ].into();

        cc.egui_ctx.set_style(style);

        // Load previous app state (if any).
        // Note that you must enable the `persistence` feature for this to work.
        if let Some(storage) = cc.storage
        {
            return eframe::get_value(storage, eframe::APP_KEY).unwrap_or_default();
        }

        Default::default()
    }

    // SCREENS

    fn screen_main_menu(&mut self, ctx: &egui::Context, ui: &mut egui::Ui, frame: &mut eframe::Frame)
    {
        ui.heading("A Toolbox for Cyberpunk RED Game Masters".to_string());
        ui.label("by Mathis Martin, aka \"MonteCrysto\"");
    }

    fn screen_jukebox(&mut self, ctx: &egui::Context, ui: &mut egui::Ui, frame: &mut eframe::Frame)
    {
        ui.heading("Jukebox here".to_string());
    }

    fn screen_rules(&mut self, ctx: &egui::Context, ui: &mut egui::Ui, frame: &mut eframe::Frame)
    {
        ui.heading("Rules here".to_string());
    }

    fn screen_turn_order(&mut self, ctx: &egui::Context, ui: &mut egui::Ui, frame: &mut eframe::Frame)
    {
        ui.heading("Turn Order here".to_string());
    }

    fn screen_net_architectures(&mut self, ctx: &egui::Context, ui: &mut egui::Ui, frame: &mut eframe::Frame)
    {
        if !self.curr_net.is_none()
        {
            let width = frame.info().window_info.size.x-600.0;
            let height = frame.info().window_info.size.y-75.0;

            ui.horizontal(|ui|
            {
                // side control buttons
                egui::Area::new("NET controls").fixed_pos(egui::pos2(240.0, 50.0)).show(ctx, |ui|
                {
                    ui.set_width(250.0);
                    ui.set_height(height);

                    ui.heading("Subnet ID");
                    ui.text_edit_singleline(&mut self.db_address);
                    ui.add_space(20.0);

                    ui.heading("Difficulty");
                    ui.horizontal(|ui|
                    {
                        if ui.button("1").clicked()
                        {
                            // globals::NET.unwrap().set_diff(1);
                        }
                        if ui.button("2").clicked()
                        {
                            
                        }
                        if ui.button("3").clicked()
                        {
                            
                        }
                        if ui.button("4").clicked()
                        {
                            
                        }
                    });

                    ui.add_space(20.0);
                    if ui.button("Reroll").clicked()
                    {
                        let mut tmp: net::NetArchitecture = net::NetArchitecture::default();
                        tmp.initialize("New Subnet".to_string(), 1u8);
                        self.curr_net = Some(tmp);
                    }
            
                    if ui.button("Delete").clicked()
                    {
                        self.curr_net = None;
                    }
                });

                ui.add_space(275.0);

                // central NET display

                let dim: f32 = 75.0;
                let dim_2D: egui::Vec2 = egui::Vec2::new(dim, dim);
                let flat_2D: egui::Vec2 = egui::Vec2::new(dim, dim / 3.0);

                let top_left:  egui::Pos2 = egui::Pos2{x: 515.0, y: 50.0};
                let bot_right: egui::Pos2 = egui::Pos2 {x: width, y: height};

                /*
                egui::ScrollArea::horizontal()
                //egui::Area::new("NET display")
                //.fixed_pos(egui::pos2(515.0, 50.0))
                .show(ui, |ui|
                {
                    // ui.set_height(frame.info().window_info.size.x / self.ui_scale - 100.0);
                    ui.set_height(height);
                    ui.set_width (width);

                    // anticipate up to 28 columns (3d6 maxed and all floors split into 15 new branches + 13 in-between)
                    let mut table = egui_extras::TableBuilder::new(ui)
                        .striped(false)
                        .cell_layout(egui::Layout::left_to_right(egui::Align::Center))
                        .column(egui_extras::Size::initial(dim).at_least(dim))
                        .column(egui_extras::Size::initial(dim).at_least(dim))
                        .column(egui_extras::Size::initial(dim).at_least(dim))
                        .column(egui_extras::Size::initial(dim).at_least(dim))
                        .column(egui_extras::Size::initial(dim).at_least(dim))
                        .column(egui_extras::Size::initial(dim).at_least(dim))
                        .column(egui_extras::Size::initial(dim).at_least(dim))
                        .column(egui_extras::Size::initial(dim).at_least(dim))
                        .column(egui_extras::Size::initial(dim).at_least(dim))
                        .column(egui_extras::Size::initial(dim).at_least(dim))
                        .column(egui_extras::Size::initial(dim).at_least(dim))
                        .column(egui_extras::Size::initial(dim).at_least(dim))
                        .column(egui_extras::Size::initial(dim).at_least(dim))
                        .column(egui_extras::Size::initial(dim).at_least(dim))
                        .column(egui_extras::Size::initial(dim).at_least(dim))
                        .column(egui_extras::Size::initial(dim).at_least(dim))
                        .column(egui_extras::Size::initial(dim).at_least(dim))
                        .column(egui_extras::Size::initial(dim).at_least(dim))
                        .column(egui_extras::Size::initial(dim).at_least(dim))
                        .column(egui_extras::Size::initial(dim).at_least(dim))
                        .column(egui_extras::Size::initial(dim).at_least(dim))
                        .column(egui_extras::Size::initial(dim).at_least(dim))
                        .column(egui_extras::Size::initial(dim).at_least(dim))
                        .column(egui_extras::Size::initial(dim).at_least(dim))
                        .column(egui_extras::Size::initial(dim).at_least(dim))
                        .column(egui_extras::Size::initial(dim).at_least(dim))
                        .column(egui_extras::Size::initial(dim).at_least(dim))
                        .column(egui_extras::Size::initial(dim).at_least(dim))
                        .resizable(false);

                    table.body(|mut body|
                    {
                        for fl in 1 ..= self.curr_net.as_ref().unwrap().nb_floors
                        {
                            body.row(dim, |mut row|
                            {
                                row.col(|ui|
                                {
                                    ui.heading(fl.to_string());
                                });

                                for br in 1 ..= self.curr_net.as_ref().unwrap().nb_branches
                                {
                                    // the floor in itself
                                    row.col(|ui|
                                    {
                                        // is there a floor?
                                        if self.curr_net.as_ref().unwrap().net_matrix[usize::from(br-1)][usize::from(fl-1)] != 0
                                        {
                                            tex::TEX_NET_FLOOR.show_size(ui, dim_2D);
                                        }

                                        // if not, is there a floor after that?
                                        else if fl < self.curr_net.as_ref().unwrap().nb_floors && self.curr_net.as_ref().unwrap().net_matrix[usize::from(br-1)][usize::from(fl)] != 0
                                        {
                                            tex::TEX_NET_TURN.show_size(ui, dim_2D);
                                        }

                                        // if not, is there a turn down the line?
                                        // dirty, made this way because we can't use curr_net methods
                                        else
                                        {
                                            // we need to be before nb_floors-3 because there can't be a split 3 before the end
                                            if fl <= self.curr_net.as_ref().unwrap().nb_floors - 3
                                            {
                                                // is there a new branch somewhere past the current one?
                                                for bbr in (br+1) ..= self.curr_net.as_ref().unwrap().nb_branches
                                                {
                                                    // if we hit a floor, abort
                                                    if self.curr_net.as_ref().unwrap().net_matrix[usize::from(bbr-1)][usize::from(fl-1)] != 0
                                                    {
                                                        break;
                                                    }

                                                    // we need a floor with nothing, and something on the next floor
                                                    if self.curr_net.as_ref().unwrap().net_matrix[usize::from(bbr-1)][usize::from(fl-1)] == 0
                                                        && self.curr_net.as_ref().unwrap().net_matrix[usize::from(bbr-1)][usize::from(fl)] != 0
                                                    {
                                                        tex::TEX_NET_HLINE.show_size(ui, dim_2D);
                                                    }
                                                }
                                            }
                                        }
                                    });
                                    
                                    // the link between splitting branches
                                    row.col(|ui|
                                    {
                                        // turn
                                        if fl < self.curr_net.as_ref().unwrap().nb_floors && br < self.curr_net.as_ref().unwrap().nb_branches
                                            && self.curr_net.as_ref().unwrap().net_matrix[usize::from(br)][usize::from(fl-1)] == 0
                                            && self.curr_net.as_ref().unwrap().net_matrix[usize::from(br)][usize::from(fl)  ] != 0
                                        {
                                            tex::TEX_NET_HLINE.show_size(ui, dim_2D);
                                        }

                                        // line, same as above
                                        else
                                        {
                                            // we need to be before nb_floors-3 because there can't be a split 3 before the end
                                            if fl <= self.curr_net.as_ref().unwrap().nb_floors - 3
                                            {
                                                // is there a new branch somewhere past the current one?
                                                for bbr in (br+1) ..= self.curr_net.as_ref().unwrap().nb_branches
                                                {
                                                    // if we hit a floor, abort
                                                    if self.curr_net.as_ref().unwrap().net_matrix[usize::from(bbr-1)][usize::from(fl-1)] != 0
                                                    {
                                                        break;
                                                    }

                                                    // we need a floor with nothing, and something on the next floor
                                                    if self.curr_net.as_ref().unwrap().net_matrix[usize::from(bbr-1)][usize::from(fl-1)] == 0
                                                        && self.curr_net.as_ref().unwrap().net_matrix[usize::from(bbr-1)][usize::from(fl)] != 0
                                                    {
                                                        tex::TEX_NET_HLINE.show_size(ui, dim_2D);
                                                    }
                                                }
                                            }
                                        }
                                    });
                                }
                            });
                            body.row(flat_2D.y, |mut row|
                            {
                                row.col(|ui|
                                {
                                    // leave space for floor numbers
                                });

                                for br in 1 ..= self.curr_net.as_ref().unwrap().nb_branches
                                {
                                    // link to next floor if there is any
                                    row.col(|ui|
                                    {
                                        if fl < self.curr_net.as_ref().unwrap().nb_floors
                                            && self.curr_net.as_ref().unwrap().net_matrix[usize::from(br-1)][usize::from(fl)] != 0
                                        {
                                            tex::TEX_NET_VLINE.show_size(ui, flat_2D);
                                        }
                                    });

                                    row.col(|ui|
                                    {
                                        // empty col
                                    });
                                }
                            });
                        }
                    });
                });
                */

                // Grid version

                egui::containers::ScrollArea::new([true, false])
                .auto_shrink([false;2])
                // .show_viewport(ui, |ui, egui::Rect {min: top_left, max: bot_right}|
                // .current_pos(top_left)
                .show(ui, |ui|
                {
                    // ui.set_width (width);
                    // ui.set_height(height);

                    egui::Grid::new("NET Display")
                    .striped(false)
                    .spacing(egui::Vec2::ZERO)
                    .show(ui, |ui|
                    {
                        // ui.set_height(frame.info().window_info.size.y -  75.0);
                        // ui.set_width (frame.info().window_info.size.x - 600.0);

                        // iterate over floors
                        for fl in 1 ..= self.curr_net.as_ref().unwrap().nb_floors
                        {
                            // 1. HEADER COL

                            ui.heading(fl.to_string());

                            for br in 1 ..= self.curr_net.as_ref().unwrap().nb_branches
                            {
                                // 2. BRANCH COL

                                // is there a floor?
                                if self.curr_net.as_ref().unwrap().net_matrix[usize::from(br-1)][usize::from(fl-1)] != 0
                                {
                                    tex::TEX_NET_FLOOR.show_size(ui, dim_2D);
                                }

                                // if not, is there a floor after that?
                                else if fl < self.curr_net.as_ref().unwrap().nb_floors && self.curr_net.as_ref().unwrap().net_matrix[usize::from(br-1)][usize::from(fl)] != 0
                                {
                                    tex::TEX_NET_TURN.show_size(ui, dim_2D);
                                }

                                // if not, is there a turn down the line?
                                // dirty, made this way because we can't use curr_net methods
                                // we need to be before nb_floors-3 because there can't be a split 3 before the end
                                else if fl <= self.curr_net.as_ref().unwrap().nb_floors - 3
                                {
                                    // is there a new branch somewhere past the current one?
                                    for bbr in (br+1) ..= self.curr_net.as_ref().unwrap().nb_branches
                                    {
                                        // if we hit a floor, abort
                                        if self.curr_net.as_ref().unwrap().net_matrix[usize::from(bbr-1)][usize::from(fl-1)] != 0
                                        {
                                            ui.heading("");
                                            break;
                                        }

                                        // we need a floor with nothing, and something on the next floor
                                        if self.curr_net.as_ref().unwrap().net_matrix[usize::from(bbr-1)][usize::from(fl-1)] == 0
                                            && self.curr_net.as_ref().unwrap().net_matrix[usize::from(bbr-1)][usize::from(fl)] != 0
                                        {
                                            tex::TEX_NET_HLINE.show_size(ui, dim_2D);
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    ui.heading("");
                                }
                                
                                // 3. LINK COL

                                // draw an angle
                                if fl < self.curr_net.as_ref().unwrap().nb_floors && br < self.curr_net.as_ref().unwrap().nb_branches
                                    && self.curr_net.as_ref().unwrap().net_matrix[usize::from(br)][usize::from(fl-1)] == 0
                                    && self.curr_net.as_ref().unwrap().net_matrix[usize::from(br)][usize::from(fl)  ] != 0
                                {
                                    tex::TEX_NET_HLINE.show_size(ui, dim_2D);
                                }

                                // draw a line
                                else if fl <= self.curr_net.as_ref().unwrap().nb_floors - 3
                                {
                                    // is there a new branch somewhere past the current one?
                                    for bbr in (br+1) ..= self.curr_net.as_ref().unwrap().nb_branches
                                    {
                                        // if we hit a floor, abort
                                        if self.curr_net.as_ref().unwrap().net_matrix[usize::from(bbr-1)][usize::from(fl-1)] != 0
                                        {
                                            ui.heading("");
                                            break;
                                        }

                                        // we need a floor with nothing, and something on the next floor
                                        if self.curr_net.as_ref().unwrap().net_matrix[usize::from(bbr-1)][usize::from(fl-1)] == 0
                                            && self.curr_net.as_ref().unwrap().net_matrix[usize::from(bbr-1)][usize::from(fl)] != 0
                                        {
                                            tex::TEX_NET_HLINE.show_size(ui, dim_2D);
                                            break;
                                        }
                                    }
                                }
                                
                                // draw nothing if we still have branches left
                                else if br < self.curr_net.as_ref().unwrap().nb_branches
                                {
                                    ui.heading("");
                                }
                            }

                            // END OF FIRST ROW
                            ui.end_row();

                            // 1. HEADER COL

                            ui.heading("");

                            for br in 1 ..= self.curr_net.as_ref().unwrap().nb_branches
                            {
                                // 2. LINK COL

                                // link to next floor if there is any
                                if fl < self.curr_net.as_ref().unwrap().nb_floors
                                    && self.curr_net.as_ref().unwrap().net_matrix[usize::from(br-1)][usize::from(fl)] != 0
                                {
                                    tex::TEX_NET_VLINE.show_size(ui, flat_2D);
                                }
                                else
                                {
                                    ui.heading("");
                                }
                                
                                // 3. EMPTY COL

                                if br < self.curr_net.as_ref().unwrap().nb_branches
                                {
                                    ui.heading("");
                                }
                            }

                            // END OF SECOND ROW
                            ui.end_row();
                        }
                    });
                });
            });
        }
    }

    fn screen_maps(&mut self, ctx: &egui::Context, ui: &mut egui::Ui, frame: &mut eframe::Frame)
    {
        ui.heading("Maps here".to_string());
    }

    fn screen_character_sheets(&mut self, ctx: &egui::Context, ui: &mut egui::Ui, frame: &mut eframe::Frame)
    {
        ui.heading("Character Sheets here".to_string());
    }

    fn screen_about(&mut self, ctx: &egui::Context, ui: &mut egui::Ui, frame: &mut eframe::Frame)
    {
        ui.heading("Cyberpunk RED Toolbox".to_string());
        ui.label("by Mathis Martin, aka \"MonteCrysto\"");

        ui.add_space(20.0);
        ui.heading("    I. Preamble");
        ui.add_space(20.0);
        ui.label("The \"Cyberpunk RED Toolbox\" is a utility software designed to help Game Masters in managing their Cyberpunk RED campaigns.");

        ui.add_space(20.0);
        ui.heading("    II. Ownership issues");
        ui.add_space(20.0);
        ui.label("    \"Cyberpunk RED\" and \"Cyberpunk 2077\" belong to R. Talsorian Games, CD Projekt Red and their original creators. This work is not affiliated with the aforementioned people and companies.");
        ui.add_space(20.0);
        ui.label("    The use of said material has been done for entertainment purposes only, and no copyright infringement was intended. The author does not earn anything from the work he has done himself in developping this piece of software.");
        ui.add_space(20.0);
        ui.label("    If, in spite of that, the owners of the original media used feel like it and as a proof of good faith and respect for their work, the author of this utility is ready to stop its distribution beside personal use by preventing the {a=https://gitlab.com/monte.crysto31/cyberpunk-red-gm-toolbox}GitLab repository it is currently hosted on from being accessed by others than himself.");

        ui.add_space(20.0);
        ui.heading("    III. Software used");
        ui.add_space(20.0);
        ui.label("    Made with rust and egui.");
    }

    fn screen_settings(&mut self, ctx: &egui::Context, ui: &mut egui::Ui, frame: &mut eframe::Frame)
    {
        ui.horizontal(|ui|
        {
            ui.vertical(|ui|{
                ui.heading("Graphics".to_string());
                ui.add_space(20.0);
                // ui.label("Vertical screen resolution".to_string());
                // ui.add(egui::Slider::new(&mut self.resolution, 480f32 ..= 4320f32).text(""));
                ui.label("UI scaling".to_string());
                ui.add(egui::Slider::new(&mut self.ui_scale, 0.7 ..= 2.0)
                    .text("").step_by(0.1));
                ui.add_space(20.0);
                
                ui.add_space(20.0);
                ui.heading("Sound".to_string());
                ui.add_space(20.0);
                ui.label("Music volume".to_string());
                ui.add(egui::Slider::new(&mut self.music_volume, 0.0f32 ..= 1.0f32)
                    .step_by(0.1).text(""));
                ui.label("SFX volume".to_string());
                ui.add(egui::Slider::new(&mut self.sfx_volume,   0.0f32 ..= 1.0f32)
                    .step_by(0.1).text(""));
            });

            ui.add_space(20.0);
            ui.separator();
            ui.add_space(20.0);

            ui.vertical(|ui|
            {
                ui.horizontal(|ui|
                {
                    ui.heading("MongoDB Database".to_string());
                    if !self.db_handle.as_ref().is_none()
                    {
                        // ui.heading(self.db_handle.as_ref().unwrap().name());
                        ui.heading("(CONNECTED)");
                    }
                    else
                    {
                        ui.heading("(DISCONNECTED)");
                    }
                });
                ui.add_space(20.0);
                ui.label("Address");
                ui.text_edit_singleline(&mut self.db_address);
                ui.add_space(20.0);
                ui.label("DB Name");
                ui.text_edit_singleline(&mut self.db_name);
                ui.add_space(20.0);
                ui.label("Username");
                ui.text_edit_singleline(&mut self.db_user);
                ui.add_space(20.0);
                ui.label("Password");
                ui.text_edit_singleline(&mut self.db_pwd);
            });
        });

        ui.add_space(20.0);
        ui.horizontal(|ui|
            {
                // apply all changes
                if ui.button("Apply").clicked()
                {
                    self.db_handle = None;
                    self.db_handle = utils::db_connect(
                        self.db_address.as_str(),
                        self.db_name.as_str(),
                        self.db_user.as_str(),
                        self.db_pwd.as_str()
                    );
                    // self.ui_scale = self.resolution / 1080.0f32;
                    ctx.set_pixels_per_point(self.ui_scale);
                }

                if ui.button("Reset").clicked()
                {
                    self.resolution = self.ui_scale * 1080.0f32;
                }
            });
    }
    
    // PANELS

    fn render_top_panel(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame, frame: egui::containers::Frame)
    {
        egui::TopBottomPanel::top("top_panel").frame(frame).show(ctx, |ui|
        {
            // The top panel is often a good place for a menu bar:
            egui::menu::bar(ui, |ui|
            {
                ui.menu_button("CPR", |ui|
                {
                    if ui.button("Fullscreen").clicked()
                    {
                        _frame.set_fullscreen(!self.is_fullscreen);
                        self.is_fullscreen = !self.is_fullscreen;
                    }

                    if ui.button("Quit").clicked()
                    {
                        _frame.set_fullscreen(false);
                        self.is_fullscreen = false;
                        self.first_frame = true;
                        _frame.close();
                    }
                });

                ui.menu_button("Jukebox", |ui|
                {
                    if ui.button("New track").clicked()
                    {
                        self.menu_id = 1;
                    }

                    if ui.button("Clear playlist").clicked()
                    {
                        
                    }
                });

                ui.menu_button("Rules", |ui|
                {
                    if ui.button("New").clicked()
                    {
                        self.menu_id = 2;
                    }
                });

                ui.menu_button("Turn Order", |ui|
                {
                    if ui.button("Clear").clicked()
                    {
                        
                    }
                });

                ui.menu_button("SubNETs", |ui|
                {
                    if ui.button("New").clicked()
                    {
                        self.menu_id = 4;
                        self.curr_net = Some(net::NetArchitecture::default());
                    }

                    if ui.button("Load").clicked()
                    {
                        self.menu_id = 4;
                    }
                });

                ui.menu_button("Maps", |ui|
                {
                    if ui.button("New").clicked()
                    {
                        self.menu_id = 5;
                    }
                });

                ui.menu_button("Characters", |ui|
                {
                    if ui.button("New").clicked()
                    {
                        self.menu_id = 6;
                    }

                    if ui.button("Load").clicked()
                    {
                        self.menu_id = 6;
                    }
                });
            });
        });
    }

    fn render_side_panel(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame, frame: egui::containers::Frame)
    {
        egui::SidePanel::left("side_panel").frame(frame).show(ctx, |ui|
        {
            ui.set_min_width(200.0);
            
            ui.vertical(|ui|
            {
                ui.set_height(75.0);
                let title: String = globals::MENUS.get(&self.menu_id).unwrap_or(&"ERROR: invalid menu_id".to_string()).to_string();
                ui.heading(title);
            });

            ui.vertical_centered_justified(|ui|
            {
                for id in [1, 2] {
                    if ui.button(globals::MENUS.get(&id).unwrap_or(&"ERROR: invalid menu_id".to_string())).clicked() {
                        self.menu_id = id;
                    }
                }

                ui.add_space(20f32);
                for id in [3, 4, 5, 6] {
                    if ui.button(globals::MENUS.get(&id).unwrap_or(&"ERROR: invalid menu_id".to_string())).clicked() {
                        self.menu_id = id;
                    }
                }

                ui.add_space(20f32);
                for id in [7, 8] {
                    if ui.button(globals::MENUS.get(&id).unwrap_or(&"ERROR: invalid menu_id".to_string())).clicked() {
                        self.menu_id = id;
                    }
                }
                
                if self.menu_id != 0
                {
                    ui.add_space(20f32);

                    if ui.button("Return").clicked()
                    {
                        self.menu_id = 0u8;
                    }
                }

                
            });
            
            ui.with_layout(egui::Layout::bottom_up(egui::Align::LEFT), |ui|
            {   
                egui::warn_if_debug_build(ui);
            });
        });
    }

    // Add the CentralPanel last!!!
    fn render_central_panel(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame, frame: egui::containers::Frame)
    {
        egui::CentralPanel::default().frame(frame).show(ctx, |ui|
        {
            ui.style_mut().spacing.slider_width = (250.0);

            // Render the correct menu according to the self.menu_id
            match self.menu_id
            {
                0 => { self.screen_main_menu(ctx, ui, _frame);         },
                1 => { self.screen_jukebox(ctx, ui, _frame);           },
                2 => { self.screen_rules(ctx, ui, _frame);             },
                3 => { self.screen_turn_order(ctx, ui, _frame);        },
                4 => { self.screen_net_architectures(ctx, ui, _frame); },
                5 => { self.screen_maps(ctx, ui, _frame);              },
                6 => { self.screen_character_sheets(ctx, ui, _frame);  },
                7 => { self.screen_about(ctx, ui, _frame);             },
                8 => { self.screen_settings(ctx, ui, _frame);          },
                _ => { ui.heading("Invalid menu, cannot render".to_string()); }
            }
        });
    }
}

impl eframe::App for TemplateApp
{
    // Called by the frame work to save state before shutdown.
    fn save(&mut self, storage: &mut dyn eframe::Storage)
    {
        eframe::set_value(storage, eframe::APP_KEY, self);
    }

    // Called each time the UI needs repainting, which may be many times per second.
    // Put your widgets into a `SidePanel`, `TopPanel`, `CentralPanel`, `Window` or `Area`.
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame)
    {
        let Self
        {
            // cpr_frame,
            is_fullscreen,
            first_frame,

            resolution,
            ui_scale,
            music_volume,
            sfx_volume,
            db_address,
            db_name,
            db_user,
            db_pwd,

            menu_id,
            db_handle,
            curr_net,
        } = self;

        // https://github.com/emilk/egui/discussions/1286
        // Dirty and innefficient, but doesn't work if done in the TemplateApp::default()
        let cpr_top_frame = egui::containers::Frame::default()
            .fill(globals::BACKGROUND)
            .rounding(egui::Rounding {nw: 0.0, ne: 0.0, sw: 0.0, se: 0.0})
            .stroke(egui::Stroke::new(2.0, globals::RED))
            .inner_margin(egui::style::Margin {left: 10., right: 10., top: 2., bottom: 2.})
            .outer_margin(egui::style::Margin {left: 0., right: 0., top: 0., bottom: 0.});
        
        let cpr_side_frame = egui::containers::Frame::default()
            .fill(globals::BACKGROUND)
            .rounding(egui::Rounding {nw: 0.0, ne: 0.0, sw: 0.0, se: 0.0})
            .stroke(egui::Stroke::new(2.0, globals::RED))
            .inner_margin(egui::style::Margin {left: 10., right: 10., top: 10., bottom: 10.})
            .outer_margin(egui::style::Margin {left:  0., right:  0., top:  0., bottom:  0.});

        let cpr_central_frame = egui::containers::Frame::default()
            .fill(globals::BACKGROUND)
            .rounding(egui::Rounding {nw: 0.0, ne: 0.0, sw: 0.0, se: 0.0})
            .stroke(egui::Stroke::new(2.0, globals::RED))
            .inner_margin(egui::style::Margin {left: 20., right: 20., top: 20., bottom: 20.})
            .outer_margin(egui::style::Margin {left:  0., right:  0., top:  0., bottom:  0.});

        if self.first_frame
        {
            ctx.set_pixels_per_point(self.ui_scale);
            self.db_handle = utils::db_connect(
                self.db_address.as_str(),
                self.db_name.as_str(),
                self.db_user.as_str(),
                self.db_pwd.as_str()
            );
            self.first_frame = false;
        }
        
        /* Area to try and render a moving background image in the future
        egui::Area::new("window background").fixed_pos(egui::pos2(0.0, 0.0)).show(ctx, |ui|
        {

        });
        */
        #[cfg(not(target_arch = "wasm32"))] // no File->Quit on web pages!
        self.render_top_panel    ( ctx, _frame, cpr_top_frame     );
        self.render_side_panel   ( ctx, _frame, cpr_side_frame    );
        self.render_central_panel( ctx, _frame, cpr_central_frame );

        /* List of Windows. Confined only to the CentralPanel!!!
        egui::Window::new("Window").show(ctx, |ui|
        {
            ui.label("Windows can be moved by dragging them.");
            ui.label("They are automatically sized based on contents.");
            ui.label("You can turn on resizing and scrolling if you like.");
            ui.label("You would normally chose either panels OR windows.");
        });
        */
    }
}
