// local
use crate::globals;

// std
use std::{
    collections::HashMap,
    ops::DerefMut,
};

// extern
use serde::{
    Serialize,
    Deserialize
};
use rand::{self, Rng};

use crate::utils;

#[derive(Deserialize, Serialize, Clone)]
pub struct Program
{
    pub name:       String,                 // readable name of program, unique
    pub nature:     String,                 // in ("Attacker, "Defender", "Booster", "Black ICE", "Demon")
    pub target:     String,                 // in ("", "Anti-Personnel", "Anti-Program")
    pub stats:      (u8, u8, u8, u8, u8),   // prg/demon: (PER/Interface, SPD/NET Actions, ATK/Combat Number, DEF, REZ)
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub enum ArchiItem
{
    Program     { id: u8 },
    Challenge   { name: String, dd: u8, contents: String },
}

#[derive(Deserialize, Serialize, Clone)]
pub struct NetArchitecture
{
    pub name:           String,         // explicit name of the subnet, unique
    pub diff:           u8,             // has value from 1 to 4
    pub nb_floors:      u8,             // total number of floors
    pub nb_branches:    u8,             // total number of branches
    pub demon:          Option<u8>,     // id of demon in the DEMONS table
    pub floors:         HashMap<(u8, u8), Vec<ArchiItem>>,
    pub branch_split:   Vec<bool>,      // does a branch split at this floor?
    pub branch_span:    Vec<(u8, u8)>,  // at which floor does each branch start and stop?
    pub turns:          Vec<bool>,      // for each intersection, does the main branch turn?correctly
    pub net_matrix:     Vec<Vec<u8>>    // a graphic representation of what branch goes where
}

impl Default for NetArchitecture
{
    fn default() -> Self
    {
        Self
        {
            name:           "New Subnet".to_string(),
            diff:           1,
            nb_floors:      0,
            nb_branches:    1,
            demon:          None,
            floors:         HashMap::new(),
            branch_split:   Vec::new(),
            branch_span:    Vec::new(),
            turns:          Vec::new(),
            net_matrix:     vec![vec![0; 1]; 1],
        }
    }
}

impl NetArchitecture
{
    // auxiliary methods to help build a random NET

    pub fn set_diff(&mut self, diff: u8)
    {
        self.diff = diff;
    }

    pub fn set_name(&mut self, name: String)
    {
        self.name = name;
    }

    pub fn add_demon(&mut self, id: u8)
    {
        self.demon = Some(id);
    }

    // reroll the contents of an existing floor
    pub fn roll_one_floor(&mut self, fl: u8, br:u8)
    {
        self.floors.remove(&(fl, br));

        // if we roll a floor between 1 and 2, it's a lobby floor rolled with 1d6
        if fl <= 2
        {
            self.floors.insert(
                (fl, br),
                globals::NET_LOBBY.get(&utils::roll_total(1, 6)).unwrap().to_vec()
            );
        }

        // else, it's a normal floor, rolled with 3d6
        else
        {
            self.floors.insert(
                (fl, br),
                globals::NET_FLOORS.get(&(utils::roll_total(3, 6), self.diff)).unwrap().to_vec()
            );
        }
    }

    // reroll the contents of all the existing floors without changing the topology
    pub fn roll_all_floors(&mut self)
    {
        // can't borrow that twice, we need to copy the floors HashMap
        let tmp_floors = self.floors.clone();
        
        // for each existing (floor, branch), we reroll it
        for key in tmp_floors.keys()
        {
            self.roll_one_floor(key.0, key.1);
        }
    }

    // add an ArchiItem to a specific floor
    pub fn add_to_floor(&mut self, item: ArchiItem, fl: u8, br: u8)
    {
        let mut floor: Option<Vec<ArchiItem>> = self.floors.remove(&(fl, br));
        
        // we add an item only if the floor exists
        if !self.floors.remove(&(fl, br)).is_none()
        {
            let mut floor = self.floors.remove(&(fl, br)).unwrap();
            floor.push(item);

            self.floors.insert((fl, br), floor);
        }

        // else we popped a floor that didn't exist, so nothing changed
        return;
    }

    // add a floor to the NET
    pub fn add_floor(&mut self, fl: u8, br: u8)
    {
        // get the right new floor depending on if we are in the lobby or not
        let mut new_floor: Vec<ArchiItem> = Vec::new();
        if fl <= 2
        {
            new_floor = globals::NET_LOBBY.get(&utils::roll_total(1, 6)).unwrap().to_vec();
        }
        else
        {
            new_floor = globals::NET_FLOORS.get(&(utils::roll_total(3, 6), self.diff)).unwrap().to_vec();
        }

        // insert the new floor
        let old_floor = self.floors.insert((fl, br), new_floor);
        
        // did we already have that floor? if yes put it back, else we had nothing and the new floor stays
        if !old_floor.is_none()
        {
            self.floors.insert((fl, br), old_floor.unwrap());
            return;
        }

        // if fl or br are outside what had been allocated for the architecture fields, we add accordingly
        if fl > self.nb_floors
        {
            println!("UNEXPECTED FLOOR ADDED!");
            self.nb_floors += 1;
            self.branch_split.push(false);
            for tmp_br in 1 ..= self.nb_branches
            {
                self.net_matrix[usize::from(tmp_br - 1)].push(0);
            }
        }
        if br > self.nb_branches
        {
            println!("UNEXPECTED BRANCH ADDED!");
            self.nb_branches += 1;
            self.branch_span.push((fl, fl));
            self.turns.push(false);
            self.net_matrix.push(vec![0; usize::from(self.nb_floors)]);
        }
    }

    // the NET matrix is nb_branches wide and nb_floors tall
    // we will use it to arrange the branches to that they don't overlap and are easier to represent in the menu
    // ATTENTION: Fortran orientation for easier manipulation of columns: net_matrix[br][fl]
    pub fn reorder_matrix(&mut self)
    {
        
        // start by regenerating a fresh matrix with only the main branch
        self.net_matrix  = vec![vec![0; usize::from(self.nb_floors)]; usize::from(self.nb_branches)];
        for fl in 1 ..= self.nb_floors
        {
            self.net_matrix[0][usize::from(fl - 1)] = 1u8;
        }

        /* DEBUG
        println!("\nBefore:");
        println!("{:?}", self.turns);
        for br in 1 ..= self.nb_branches
        {
            println!("{:?}", self.net_matrix[usize::from(br - 1)]);
        }
        */

        // first pass: rearrange according to turns
        let mut main_at:     u8 = 1;    // current horizontal position of the main branch
        let mut last_branch: u8 = 1;    // last processed branch
        let mut tmp_rows:    u8 = 0;    // rows to delete at the end
        let mut fl:          u8 = 1;    // current floor
        while (fl < self.nb_floors) && (last_branch < self.nb_branches)
        {
            // there is a new branch that splits on this floor
            if self.branch_split[usize::from(fl - 1)]
            {
                // check for any overlapping branch
                if self.net_matrix[usize::from(main_at)][usize::from(fl - 1)] != 0
                {
                    // if there is any, it is a branch we already processed and we need to offset that branch
                    self.net_matrix.insert(usize::from(main_at), vec![0; usize::from(self.nb_floors)]);
                    tmp_rows += 1;
                }

                // if the main branch turns at the current intersection, we swap all floors from fl+1 onwards
                if self.turns[usize::from(last_branch - 1)]
                {
                    // swap
                    for ffl in (fl + 1) ..= self.nb_floors
                    {
                        self.net_matrix[usize::from(main_at)    ][usize::from(ffl - 1)] = 1;
                        self.net_matrix[usize::from(main_at - 1)][usize::from(ffl - 1)] = 0;
                    }
                    
                    // the new branch (last_branch+1) continues in stead of the main one
                    for ffl in (fl + 1) ..= self.branch_span[usize::from(last_branch - 1)].1
                    {
                        self.net_matrix[usize::from(main_at - 1)][usize::from(ffl - 1)] = last_branch + 1;
                    }

                    main_at += 1;
                }

                // if the branch doesn't turn, we leave it alone and add the new branch on the side
                else
                {
                    for ffl in (fl + 1) ..= self.branch_span[usize::from(last_branch - 1)].1
                    {
                        self.net_matrix[usize::from(main_at)][usize::from(ffl - 1)] = last_branch + 1;
                    }
                }

                last_branch += 1;
            }

            fl += 1;
        }

        // remove temporary rows
        for i in 1 ..= tmp_rows
        {
            self.net_matrix.remove(usize::from(self.nb_branches));
        }
        println!("Removed {} unnecessary rows", tmp_rows);

        println!("\nNet Matrix:");
        for br in 1 ..= self.nb_branches
        {
            println!("{:?}", self.net_matrix[usize::from(br - 1)]);
        }
    }
    
    // and finally, the all-in-one NET generator!
    pub fn initialize(&mut self, name: String, diff: u8)
    {
        println!("Creating new NetArchitecture \"{}\" of difficulty {}", name, diff);
        self.name = name;
        self.diff = diff;
        self.nb_floors = utils::roll_sum(utils::roll(3, 6), 3);
        
        // we will add branches until our d10 rolls under 7, or we hit the branch limit
        // the limit of additional branches (nb_branches-1) is nb_floors-3:
        //   - 1. I've decided that branches only derive from the main branch, for simplicity
        //   - 2. RAW state that the architecture can't split on the first floor
        //   - 3. a branch "starting" at floor fl will actually split at that level, and will have floors from fl+1 onwards
        //   - 4. a branch needs at least 1 floor to exist
        //   - 5. RAW state that a branch cannot end on the last floor either
        //   - 6. according to 3, 4 and 5, a branch can only start three floors before the last one, aka a clearance of two floors after the split
        //   - 7. according to 2 and 6, no matter how many levels we have, there are 3 floors a branch cannot start from
        self.nb_branches =  1;
        while (utils::roll_total(1, 10) <= 7) && (self.nb_branches-1 < self.nb_floors-3)
        {
            self.nb_branches += 1;
        }

        println!("The new NET will have {} levels and {} branches", self.nb_floors, self.nb_branches);

        // simple net_matrix, untouched if nb_branches == 1
        self.net_matrix  = vec![vec![1; usize::from(self.nb_floors)]; 1];

        // determine at random where each branch starts and ends
        // all branches will stop at a floor between start+1 and nb_floors
        // a branch starting at branch_start[fl] will end at branch_end[fl]
        self.branch_split = vec![false;      usize::from(self.nb_floors)    ];
        self.branch_span  = vec![(0u8, 0u8); usize::from(self.nb_branches-1)];
        let mut rng = rand::thread_rng();

        // generate floors for the main branch only
        self.floors = HashMap::new();
        for fl in 1 ..= self.nb_floors
        {
            self.add_floor(fl, 1u8);
        }

        // we don't bother for the rest if we only have the main branch
        if self.nb_branches > 1
        {
            // does the main branch turn at each intersection or does it go straight?
            // true=turn, false=straight
            self.turns = vec![false; usize::from(self.nb_branches - 1)];
            for br in 0 ..= (self.nb_branches - 2)
            {
                // 1/2 chance to turn or to go straight, aren't maths beautiful?
                if utils::roll_total(1, 2) == 1
                {
                    self.turns[usize::from(br)] = true;
                }
            }

            // determine each branch's span
            // given how the number of branches was decided, the following 'while' will always complete
            let mut curr_branch = 2u8;
            while curr_branch <= self.nb_branches
            {
                // as per 7, we can only start from floor 2 onwards, and from before 2 before the last floor
                let start: u8 = rng.gen_range(2u8 ..= (self.nb_floors-2));

                if !self.branch_split[usize::from(start - 1)]
                {
                    self.branch_split[usize::from(start - 1)]         = true;
                    self.branch_span [usize::from(curr_branch - 2)].0 = start + 1;
                    self.branch_span [usize::from(curr_branch - 2)].1 = rng.gen_range((start + 1) ..= (self.nb_floors - 1));

                    curr_branch += 1;
                }
            }

            // we sort this list so that the branches appear in order of splitting
            self.branch_span.sort_by(|a, b| a.0.cmp(&b.0));
            
            for br in 2 ..= self.nb_branches
            {
                println!("Branch #{} will split at floor {} and will be present from floors {} to {}",
                br,
                        self.branch_span [usize::from(br - 2)].0 - 1,
                        self.branch_span [usize::from(br - 2)].0,
                        self.branch_span [usize::from(br - 2)].1
                );
            }

            // add all branch floors
            for br in 2 ..= self.nb_branches
            {
                for fl in (self.branch_span[usize::from(br - 2)].0) ..= (self.branch_span[usize::from(br - 2)].1)
                {
                    self.add_floor(fl, br);
                }
            }

            // arrange the net_matrix
            self.reorder_matrix();
        }

        // finish by generating all floors
        self.roll_all_floors();
    }

    pub fn clone(&mut self) -> NetArchitecture
    {
        let mut newArchi = NetArchitecture::default();

        newArchi.name           = self.name         .clone();
        newArchi.diff           = self.diff;
        newArchi.nb_floors      = self.nb_floors;
        newArchi.nb_branches    = self.nb_branches;
        newArchi.demon          = self.demon;
        newArchi.floors         = self.floors       .clone();
        newArchi.branch_split   = self.branch_split .clone();
        newArchi.branch_span    = self.branch_span  .clone();
        newArchi.turns          = self.turns        .clone();
        newArchi.net_matrix     = self.net_matrix   .clone();

        newArchi
    }

    pub fn init_from(&mut self, oldArchi: NetArchitecture)
    {
        self.name          = oldArchi.name         .clone();
        self.diff          = oldArchi.diff;
        self.nb_floors     = oldArchi.nb_floors;
        self.nb_branches   = oldArchi.nb_branches;
        self.demon         = oldArchi.demon;
        self.floors        = oldArchi.floors       .clone();
        self.branch_split  = oldArchi.branch_split .clone();
        self.branch_span   = oldArchi.branch_span  .clone();
        self.turns         = oldArchi.turns        .clone();
        self.net_matrix    = oldArchi.net_matrix   .clone();
    }
}